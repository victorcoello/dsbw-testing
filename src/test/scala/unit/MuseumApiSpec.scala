package unit

import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.mockito.Mockito
import museums.{NewObject, MuseumObject, MuseumApi, Repository}
import server.{JSON, HttpStatusCode}

class MuseumApiSpec extends FlatSpec with BeforeAndAfter {

  var repository: Repository = null
  var api: MuseumApi = null

  before {
    repository = Mockito.mock(classOf[Repository])
    api = new MuseumApi(repository)
  }

  "The Museum API" should "get an object" in {
    val id:Long = 5
    val obj = Some(MuseumObject(id, "Rembrandt", "My painting"))
    Mockito.when(repository.find(id)).thenReturn(obj)

    assert(api.getObject(s"/$id").body === obj)
    assert(api.getObject(s"/$id").status === HttpStatusCode.Ok)

  }

  // The description is not complete. It should tell the reader WHEN the api should fail
  "The Museum API" should "fail to get an object" in {
    val id:Long = 5
    val obj = None
    Mockito.when(repository.find(id)).thenReturn(obj)

    // The HTTP status already tells us the object was not found
    assert(api.getObject(s"/$id").body.get === "Not found")
    assert(api.getObject(s"/$id").status === HttpStatusCode.NotFound)

  }

  "The Museum API" should "post an object" in {
    val id:Long = 5
    var obj =  MuseumObject(id, "Rembrandt", "My painting")
    Mockito.when(repository.create("Rembrandt", "My painting")).thenReturn(obj)
    val objAux = JSON.toJSON(NewObject("Rembrandt","My painting"))

    assert(api.createObject(objAux).body.get === obj )
    assert(api.createObject(objAux).status === HttpStatusCode.Created)

    // What about the Location header??

  }

    // What can possibly go wrong? Is it ok to have 2 identical works?

  "The Museum API" should "delete an object" in {
    val id:Long = 5
    val obj = Some(MuseumObject(id, "Rembrandt", "My painting"))

    Mockito.when(repository.delete(id)).thenReturn(obj)

    assert(api.deleteObject(s"/$id").status === HttpStatusCode.Ok)

   }

}

  // Same issue with the description
  "The Museum API" should "fail to delete an object" in {
    val id:Long = 5
    val obj = None
    Mockito.when(repository.delete(id)).thenReturn(obj)

    assert(api.deleteObject(s"/$id").status === HttpStatusCode.NotFound)
    assert(api.deleteObject(s"/$id").body.get === "Not found")
  }
}
